My Profile setup centred from powershell but about other programs too.

All config and linked files, see [./Setup.ps1](setup.ps1) for commands.

# Workflow

## Powershell
Use your personal 'my' repository for publishing your own modules to re-use

1. `git clone|init` your own module.
2. `publish-module -path . -repository my`
3. `install-module <name>`

Now it will auto-load so you don't need to import-module.

Some commands you might want for clean up `get-module <name> -repositorty my -listAvailable | uninstall-module` to remove old versions and then install-module your latest one.

To re-publish the same version number you need to delete the .nupkg from the repository directory.

# Shared Code
Mine or others code that can be used unrelated to my preferences.

1. My [TerminalHelpers](https://gitlab.com/Jackbennett/terminalhelpers) are used.
2. My [FileSystemHelpers](https://gitlab.com/Jackbennett/filesystemhelpers) are used.
