[CmdletBinding(SupportsShouldProcess)]
<#
.SYNOPSIS
    Quick create for a new computer
.DESCRIPTION
    Uses file system links to put tracked git config files into OS path locations.

    If you want to test some work without changing these files you can always checkout a new git branch into another folder.
    `git worktree add -b rewrite ../rewrite-profile` makes another working folder next to this one,
    tracked by the same git repository on a new branch called rewrite.
    Follow up with `cd ../rewrite-profile` or `code --add ../rewrite-profile`

    See .NOTES for further development.
.EXAMPLE
    PS C:\> .\Setup.ps1 -WhatIf
    Use whatIf to avoid linking up your files to see what the script will do.
    
    VERBOSE: Hardlink to Powershell Profile
    What if: Performing the operation "Create missing parent folder" on target "C:\Users\Xample\Documents\PowerShell".
    What if: Performing the operation "Create Hardlink to repository profile.ps1" on target "C:\Users\Xample\Documents\PowerShell\profile.ps1".
    VERBOSE: Hardlink to Git Config
    What if: Performing the operation "Create missing parent folder" on target "C:\Users\Xample".
    What if: Performing the operation "Create Hardlink to repository .gitconfig" on target "C:\Users\Xample\.gitconfig".
.NOTES
    Suggested workflow might be to keep all code organized in the master branch;
    Make another branch per tweak if there's some changes like per machine settings.
    Then only pull in changes from the master branch, don't add to that from your checkout out working branch.
    To make changes start a feature branch from master and merge back into master, rebase working branches from master.
#>
Param(
    $PersonalRepositoryName = 'My'
)
Begin {
    $pref = $VerbosePreference
    $VerbosePreference = 'Continue'
}
Process {
    function createMyRepo(){
        Write-Verbose "Personal powershell reposity"
        $repoPath = (Join-path $env:LOCALAPPDATA "\powershell\PersonalRepositoryName-repo")
        try {
            if ($PSCmdlet.ShouldProcess($repoPath, "Register '$PersonalRepositoryName' repository")) {
                New-Item -ItemType Directory -Path $repoPath -ErrorAction SilentlyContinue
                Register-PSRepository -name $PersonalRepositoryName -SourceLocation $repoPath -PublishLocation $repoPath -InstallationPolicy Trusted -ErrorAction Stop
            }
        } catch [System.Exception] {
            Write-Warning $psitem.Exception.Message
        }
    }
    function mapConfigFiles(){
        # Add the source file to ./conf/ and create an entry here.
        $config = @(
            @{
                Description = "Powershell Profile"
                Path = $profile.CurrentUserAllHosts
                Value = 'profile.ps1'
            }
            @{
                Description = "Git Config"
                Path = Join-Path $env:USERPROFILE '.gitconfig'
                Value = '.gitconfig'
            }
            @{
                Description = "Windows Terminal"
                Path = Join-Path $env:LOCALAPPDATA 'Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json'
                Value = 'settings.json'
            }
        )
    
        Foreach($file in $config){
            Write-Verbose "Hardlink to $($file.Description)"
            $tree = split-path $file.Path
            if(-not (test-path $tree)){
                if ($PSCmdlet.ShouldProcess($tree, "Create missing parent folder")) {
                    New-item -ItemType Directory -Path $tree -force > $null
                }
            }
            if ($PSCmdlet.ShouldProcess($file.path, "Create Hardlink to repository $($file.Value)")) {
                New-Item -ItemType HardLink -Path $file.Path -Value (Join-path $PSScriptRoot $file.Value)
            }
        }
    }

    createMyRepo

    mapConfigFiles


}
End {
    $VerbosePreference = $pref
}
