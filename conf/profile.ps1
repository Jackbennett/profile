Clear-Host
try {
    Get-PSRepository -Name 'My' -ErrorAction Stop > $null
}
catch [System.Exception] {
    Write-Warning "See Profile Setup.ps1 in $PSScriptRoot to create local repo for publishing your own modules"
}

try {
    Import-Module FileSystemHelpers -ErrorAction Stop
    Set-Alias lsa Format-Directory
    Set-Alias new New-Directory
    Set-Alias lc Show-DirectoryContents

    Import-Module TerminalHelpers -ErrorAction Stop
    Set-Alias t Set-WindowTitle
    Set-Alias prompt Write-SmartLineHistoryPrompt -Scope Global
}
catch [System.IO.FileNotFoundException] {
    Write-Warning $psitem.CategoryInfo
}

Set-Alias g git
Set-Alias d docker
Set-Alias dc docker-compose

Push-Location "C:\src"
try {
    lsa
}
catch {
    # nevermind, aliased command not installed
}
